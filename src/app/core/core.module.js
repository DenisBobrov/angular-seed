(function (angular) {
    angular
        .module('app.core',[
            'templates',
            'ui.router',
            'ui.bootstrap',
            'restangular',
            'angular-loading-bar',
            'ngCookies',
            'LocalStorageModule'
        ]);
})(angular);