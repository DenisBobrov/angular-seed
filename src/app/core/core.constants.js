(function (angular){
    angular
        .module('app.core')
        // thirdparty libraries
        .constant('_', _) // lodash
    ;
})(angular);