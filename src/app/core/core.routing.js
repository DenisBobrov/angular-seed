(function (angular){
    angular
        .module('app.core')
        .config(coreRoutingConfig);

    function coreRoutingConfig($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                abstract: true,
                //url: '',
                views: {
                    'header': {
                        template: ''
                    },
                    'content': {
                        template: ''
                    },
                    'footer': {
                        template: ''
                    }
                },
                data: {
                    accessLevel: 'PUBLIC'
                }
            })
            .state('app.404', {
                url: '/404',
                views: {
                    'content@': {
                        controller: 'Error404Controller as vm',
                        templateUrl: 'core/views/404.html'
                    }
                }

            })
            .state('app.server_error', {
                url: '/server-error',
                views: {
                    'content@': {
                        controller: 'ServerErrorController as vm',
                        templateUrl: 'core/views/server-error.html'
                    }
                }
            });
    }
})(angular);