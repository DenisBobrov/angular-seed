(function (angular) {
    angular
        .module('app.core')
        .config(coreConfig);

    function coreConfig($locationProvider, cfpLoadingBarProvider, $urlMatcherFactoryProvider, localStorageServiceProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        cfpLoadingBarProvider.includeSpinner = false;

        $urlMatcherFactoryProvider.strictMode(false);

        localStorageServiceProvider.setPrefix('app');
    }
})(angular);