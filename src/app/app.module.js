(function (angular) {
    angular
        .module('app', [
            'app.core',
            'app.home'
        ]);
})(angular);
