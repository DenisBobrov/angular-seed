(function (angular){
    angular
        .module('app.security')
        .run(run);

    function run($rootScope, $state, $log, authService) {
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            if(!('data' in toState) || !('accessLevel' in toState.data)){
                var errorMsg = 'Access undefined for this state';

                $log.log(errorMsg);

                $rootScope.error = errorMsg;
                event.preventDefault();

                throw new Error(errorMsg);
            } else if (!authService.authorize(authService.accessLevels[toState.data.accessLevel])) {
                var errorMsg = 'Seems like you tried accessing a route you don\'t have access to...';

                $log.log(errorMsg);

                $rootScope.error = errorMsg;
                event.preventDefault();

                if(fromState.url === '^') {
                    if(authService.isLoggedIn()) {
                        $state.go('app.home.index');
                    } else {
                        $rootScope.error = null;
                        $state.go('app.security.login');
                    }
                }
            }
        });
    }
})(angular);