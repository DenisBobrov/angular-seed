(function (angular){
    angular
        .module('app.security')
        .directive('accessLevel', accessLevel);

    function accessLevel(authService, $log) {
        return {
            restrict: 'A',
            link: function($scope, element, attrs) {
                var prevDisp = element.css('display');
                var userRole;
                var accessLevel;

                $scope.user = authService.user;
                $scope.accessLevels = authService.accessLevels;

                $scope.$watch('user', function(user) {
                    if(user.role) {
                        userRole = user.role;
                    }

                    updateCSS();
                }, true);

                attrs.$observe('accessLevel', function(al) {
                    if(al) {
                        accessLevel = $scope.accessLevels[al];
                    }

                    updateCSS();
                });

                function updateCSS() {
                    if(userRole && accessLevel) {
                        if(!authService.authorize(accessLevel, userRole)) {
                            element.css('display', 'none');
                        } else {
                            element.css('display', prevDisp);
                        }
                    }
                }
            }
        };
    }
})(angular);
