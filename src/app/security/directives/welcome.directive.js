(function (angular){
    angular
        .module('app.security')
        .directive('welcome', welcome);

    function welcome() {
        return {
            restrict: 'AE',
            scope: {},
            controller: function ($scope, $log, authService) {
                $scope.user = authService.user;
            },
            templateUrl: 'security/views/directive/welcome.html'
        }
    }
})(angular);