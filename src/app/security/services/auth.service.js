(function (angular){
    angular
        .module('app.security')
        .factory('authService', AuthServiceFactory);

    function AuthServiceFactory($rootScope, _, authHelper, UserEvents, userService) {
        var userRoles = authHelper.userRoles;
        var accessLevels = authHelper.accessLevels;
        var currentUser = userService.getLoggedInUser() || createDefaultUser();

        // event handling
        $rootScope.$on(UserEvents.LOGGED_IN, onUserLoggedInHandler);
        $rootScope.$on(UserEvents.LOGGED_OUT, onUserLoggedOutHandler);

        // public api
        return {
            authorize: authorize,
            isLoggedIn: isLoggedIn,
            accessLevels: accessLevels,
            userRoles: userRoles,
            user: currentUser
        };

        function onUserLoggedInHandler(event, user) {
            changeUser(user);
        }

        function onUserLoggedOutHandler(event) {
            changeUser(createDefaultUser());
        }

        function createDefaultUser() {
            return {
                username: '',
                role: userRoles.PUBLIC
            };
        }

        function changeUser(user) {
            angular.extend(currentUser, user);
        }

        function authorize(accessLevel, role) {
            if (angular.isUndefined(role)) {
                role = currentUser.role;
            }

            return accessLevel.bitMask & role.bitMask;
        }

        function isLoggedIn() {
            return currentUser.role.title === userRoles.USER.title || currentUser.role.title === userRoles.ADMIN.title;
        }
    }
})(angular);