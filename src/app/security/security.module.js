(function (angular){
    angular
        .module('app.security', [
            'app.user'
        ]);
})(angular);