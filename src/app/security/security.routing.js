(function (angular){
    angular
        .module('app.security')
        .config(securityRoutingConfig);

    function securityRoutingConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.security', {
                abstract: true,
                url: '/security',
                views: {
                    'header@': {
                        template: ''
                    },
                    'footer@': {
                        template: ''
                    }
                },
                data: {
                    accessLevel: 'ANON'
                }
            })
            .state('app.security.login', {
                url: '/login',
                views: {
                    'content@': {
                        controller: 'LoginController as vm',
                        templateUrl: 'security/views/login.html'
                    }
                }
            })
            .state('app.security.logout', {
                url: '/logout',
                views: {
                    'content@': {
                        controller: 'LogoutController',
                        template: 'test'
                    }
                },
                data: {
                    accessLevel: 'USER'
                }
            });

        $urlRouterProvider.otherwise('/404')
    }
})(angular);