(function (angular){
    angular
        .module('app.security')
        .controller('LoginController', LoginController);

    function LoginController($rootScope, $state, $log, userService) {
        var vm = this;

        vm.error = '';
        vm.user = {
            username: '',
            password: '',
            rememberMe: false
        };

        // actions
        vm.login = login;

        function login() {
            userService.login(vm.user).then(
                function (res) {
                    $state.go('app.home.index');
                },
                function (err) {
                    vm.error = 'Bad credentials.'
                }
            );
        }
    }
})(angular);