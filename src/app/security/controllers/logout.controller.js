(function (angular){
    angular
        .module('app.security')
        .controller('LogoutController', LogoutController);

    function LogoutController($log, $state, userService) {
        var vm = this;

        // check if user navigates to logout url
        if ($state.current.name === 'app.security.logout') {
            logout();
        }

        // actions
        vm.logout = logout;

        function logout() {
            userService.logout().then(
                function () {
                    $log.log('successfully logged out.');
                    $state.go('app.security.login');
                },
                function () {
                    $rootScope.error = 'Failed to logout.'
                }
            );
        }
    }
})(angular);