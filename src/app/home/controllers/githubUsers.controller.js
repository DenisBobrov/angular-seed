(function (angular) {
    angular
        .module('app.home')
        .controller('GithubUsersController', GithubUsersController);

    function GithubUsersController($log, githubUsersService) {
        var vm = this;

        vm.users = [];

        init();

        function init() {
            githubUsersService.getUsers().then(function (response) {
                vm.users = response.data;
            });
        }
    }
})(angular);