(function (angular) {
    angular
        .module('app.home')
        .factory('githubUsersService', GithubUsersServiceFactory);

    function GithubUsersServiceFactory($http, $log) {
        return {
            getUsers: getUsers
        };

        function getUsers() {
            return $http.get('https://api.github.com/users');
        }
    }
})(angular);