(function (angular){
    angular
        .module('app.home')
        .config(homeRoutingConfig);

    function homeRoutingConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app.home', {
                abstract: true,
                url: '/home',
                views: {
                    'header@': {
                        templateUrl: 'home/views/header.html'
                    }
                },
                data: {
                    accessLevel: 'PUBLIC'
                }
            })
            .state('app.home.index', {
                url: '/',
                views: {
                    'content@': {
                        controller: 'HomeController as vm',
                        templateUrl: 'home/views/home.html'
                    }
                }
            })
            .state('app.home.about', {
                url: '/about',
                views: {
                    'content@': {
                        controller: 'AboutController as vm',
                        templateUrl: 'home/views/about.html'
                    }
                },
                data: {
                    accessLevel: 'USER'
                }
            })
            .state('app.home.github_users', {
                url: '/github/users',
                views: {
                    'content@': {
                        controller: 'GithubUsersController as vm',
                        templateUrl: 'home/views/github-users.html'
                    }
                },
                data: {
                    accessLevel: 'ADMIN'
                }
            });

        $urlRouterProvider.when('/', '/home/');
    }
})(angular);