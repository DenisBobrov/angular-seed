(function (angular){
    angular
        .module('app.user')
        .constant('UserEvents', {
            LOGGED_IN: 'user:loggedIn',
            LOGGED_OUT: 'user:loggedOut'
        });
})(angular);