(function (angular){
    angular
        .module('app.user')
        .factory('userService', UserServiceFactory);

    function UserServiceFactory($q, $rootScope, _, localStorageService, authHelper, UserEvents) {
        // initial set of users
        var users = [
            { username: 'admin', password: 'admin', role: authHelper.userRoles.ADMIN },
            { username: 'user', password: 'user', role: authHelper.userRoles.USER }
        ];

        return {
            login: login,
            logout: logout,
            getLoggedInUser: getLoggedInUser
        };

        function login(user) {
            var deferred = $q.defer();

            var foundUser = _.find(users, {username: user.username, password: user.password});

            if (!angular.isUndefined(foundUser)) {
                //changeUser(foundUser); TODO: Fix this
                $rootScope.$broadcast(UserEvents.LOGGED_IN, foundUser);

                localStorageService.set('user', foundUser);
                deferred.resolve(foundUser);
            } else {
                deferred.reject('Bad credentials.');
            }

            return deferred.promise;
        }

        function logout() {
            // make an api call here
            var deferred = $q.defer();

            localStorageService.remove('user');

            //changeUser(createDefaultUser()); TODO: Fix this
            $rootScope.$broadcast(UserEvents.LOGGED_OUT);

            deferred.resolve('Successfully logged out.')

            return deferred.promise;
        }

        function getLoggedInUser() {
            return localStorageService.get('user');
        }
    }
})(angular);