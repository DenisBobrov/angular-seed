## Quick Start

Install Node.js and then:

```sh
$ git clone git@bitbucket.org:DenisBobrov/angular-seed.git
$ cd angular-seed
$ sudo npm -g install gulp bower
$ npm install
$ bower install
$ gulp connect watch
```

Finally, open `http://localhost:8080/` in your browser.
